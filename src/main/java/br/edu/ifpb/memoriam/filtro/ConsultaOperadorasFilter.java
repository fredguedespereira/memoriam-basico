package br.edu.ifpb.memoriam.filtro;

import java.io.IOException;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.DispatcherType;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;

import br.edu.ifpb.memoriam.entidade.Operadora;

/**
 * Servlet Filter implementation class ConsultaOperadorasFilter
 */
@WebFilter(urlPatterns = { "/contato/cadastro.jsp" }, dispatcherTypes = { DispatcherType.REQUEST,
		DispatcherType.FORWARD })
public class ConsultaOperadorasFilter implements Filter {

	public void destroy() {
	}

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		// Cria a fábrica de EntityManagers e uma EntityManager
		EntityManagerFactory emf = null;
		try {
			emf = Persistence.createEntityManagerFactory("memoriam");
			EntityManager em = emf.createEntityManager();
			List<Operadora> operadoras = em.createQuery("select o from Operadora o").getResultList();
			request.setAttribute("operadoras", operadoras);
		} finally {
			emf.close();
		}
		chain.doFilter(request, response);
	}

	public void init(FilterConfig fConfig) throws ServletException {
	}

}
